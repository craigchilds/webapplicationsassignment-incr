package com.cc419.incr.ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;

/**
 * EJB representing a DonationList
 * @author craigchilds
 */
@Singleton
public class DonationList {
    
    /**
     * Store a list of donations
     */
    List<Donation> donations;
    
    /**
     * Construct an empty donation list
     */
    public DonationList()
    {
        donations = new ArrayList<>();
    }
    
    /**
     * Add a donation to the list
     * @param d 
     */
    public void add(Donation d)
    {
        donations.add(d);
    }
    
}
