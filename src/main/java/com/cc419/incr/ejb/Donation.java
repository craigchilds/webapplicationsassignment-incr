package com.cc419.incr.ejb;

import java.util.Date;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Donation
 * @author craigchilds
 */
@XmlRootElement(name = "donation")
public class Donation {
    
    String charity, cause;
    double amount;
    Date date;
    
    public Donation() {}
    
    /**
     * Get the charity for this donation
     * @return charity
     */
    @XmlAttribute
    public String getCharity()
    {
        return charity;
    }
    
    /**
     * Set the charity for this donation
     * @param charity 
     */
    public void setCharity(String charity)
    {
        this.charity = charity;
    }
    
    /**
     * Get the cause with which this donation
     * is for.
     * @return cause 
     */
    @XmlAttribute
    public String getCause() 
    {
        return cause;
    }
    
    /**
     * Set the cause of the donation
     * @param cause 
     */
    public void setCause(String cause) 
    {
        this.cause = cause;
    }

    /**
     * Get the amount which is being donated
     * @return amount
     */
    @XmlAttribute
    public double getAmount() 
    {
        return amount;
    }
    
    /**
     * Set the amount for the donation
     * @param amount 
     */
    public void setAmount(double amount) 
    {
        this.amount = amount;
    }

    /**
     * Get the date with which the donation was made
     * @return date
     */
    @XmlAttribute
    public Date getDate() 
    {
        return date;
    }
    
    /**
     * Set the date for the donation
     * @param date 
     */
    public void setDate(Date date) 
    {
        this.date = date;
    }
}