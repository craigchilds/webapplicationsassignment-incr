package com.cc419.incr;

import com.cc419.incr.ejb.Donation;
import com.cc419.incr.ejb.DonationList;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Represent A charity registry
 * @author craigchilds
 */
@Singleton
@Path("/charity")
public class CharityRegistry {
    
    @EJB
    DonationList donations;
    
    /**
     * Construct the service
     */
    public CharityRegistry() 
    {
        donations = new DonationList();
    }
    
    /**
     * Handle requests to verify a charity
     * @param cid
     * @return response
     */
    @GET
    @Path("/{cid}")
    @Produces({"application/json", "application/xml"})
    public Response getCharity(@PathParam("cid") String cid)
    {
        System.out.println("Received request for charity registration : " + cid);
        if(cid.equals("")) {
            return Response.noContent().build();
        }
        
        return Response.ok().build();
    }
    
    /**
     * Handle requests to register a donation
     * @param donation
     * @param context
     * @return response
     */
    @POST
    @Path("/donation")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public Response postDonation(Donation donation, @Context UriInfo context)
    {
        System.out.println("Received request for donation");
        if(donation == null) return Response.notModified().build();
        
        donations.add(donation);
        return Response.ok(donation).build();
    }
}
